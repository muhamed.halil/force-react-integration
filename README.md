# Force.com - React.js integration for Softhouse

## Deployment guide

### 0. Install git
	https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

### 1. Install nvm
	https://github.com/creationix/nvm

### 2. Install latest version of node and npm, e.g.
	nvm list available
	nvm install 7.10.1
	nvm use 7.10.1

### 3. Install webpack globally (for building react app)
	npm i -g webpack

### 4. Pull repo
	git clone git@gitlab.com:muhamed.halil/force-react-integration.git

### 5. Install node packages(might take a while)
	cd force-react-integration
	npm install

### 6. Run webpack to build nodeapp (if you want to futher make changes, bundle is compiled and deployed)
	webpack

### 7. Run server
	node server/server.js

### 8. Open in browser for testing:
	http://localhost:3000

- - -

## About
Sample integration project between Salesforce and a React.js application.

This application demonstrates the following concepts:
- use of the [Salesforce Node client](https://github.com/pozil/salesforce-node-client) library for Force.com OAuth 2.0 authentication and data interaction
- use of the [Salesforce Lightning Design System](https://www.lightningdesignsystem.com) (SLDS) in a web application (all the CSS is provided by SLDS)


## Screenshots
<div style="text-align:center;">
	<img src="/screenshots/login.png?raw=true" width="45%" alt="Login screen"/>
	<img src="/screenshots/main.png?raw=true" width="45%" alt="Main screen"/>
</div>

## My introduction to Salesforce - video and links:
- Introduction to Salesforce video: [Sarajevo Openweb Meetup - Introduction to Salesforce - 22/08/2017](https://www.youtube.com/watch?v=WTYkQVEF_ng)
- introduction to Salesforce HTML presentation: [Gitlab](https://gitlab.com/muhamed.halil/introduction-to-salesforce)

- - -

## For professional approach to app building, following concepts should be applied as mcuh as possible:

### 1. 12-factor app:
- [https://12factor.net/](https://12factor.net/)

### 2. Design patterns
- [https://sourcemaking.com/design_patterns](https://sourcemaking.com/design_patterns)

### 3. Solid principles
- [https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design))

### 4. Algorithms and data structures
- [https://www.cs.usfca.edu/~galles/visualization/Algorithms.html](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)
- [https://www.programcreek.com/2012/11/top-10-algorithms-for-coding-interview/](https://www.programcreek.com/2012/11/top-10-algorithms-for-coding-interview/)
- [https://www.techiedelight.com/data-structures-and-algorithms-interview-questions-stl/](https://www.techiedelight.com/data-structures-and-algorithms-interview-questions-stl/)
- [https://web.engr.illinois.edu/~jeffe/teaching/algorithms/](https://web.engr.illinois.edu/~jeffe/teaching/algorithms/)
- [https://web.engr.illinois.edu/~jeffe/teaching/algorithms/notes/](https://web.engr.illinois.edu/~jeffe/teaching/algorithms/notes/)
- [https://drive.google.com/drive/folders/0B3TYHdEDoVIbY2RHV2I1QXg3MUk](https://drive.google.com/drive/folders/0B3TYHdEDoVIbY2RHV2I1QXg3MUk)
- [https://drive.google.com/drive/folders/0B3TYHdEDoVIbLXpiazVxaHk1TlE](https://drive.google.com/drive/folders/0B3TYHdEDoVIbLXpiazVxaHk1TlE)
