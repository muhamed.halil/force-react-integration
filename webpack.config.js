const path = require('path');

module.exports = {
  entry: './app/scripts/app.js',
  output: {
    path: __dirname + '/public/scripts',
    filename: 'bundle.min.js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['react', 'es2015']
        }
      }
    ]
  }
};